package com.edatalia.signplysdk.demo.dni

import android.net.Uri

data class ConfigDniState(
    val fileUri: Uri = Uri.parse(""),
    val can: String = "",
    val enableTimestamp: Boolean = false,
    val showSuccessDialog: Boolean = false
)