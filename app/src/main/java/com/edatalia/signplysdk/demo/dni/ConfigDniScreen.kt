package com.edatalia.signplysdk.demo.dni

import android.net.Uri
import android.widget.Toast
import androidx.activity.compose.rememberLauncherForActivityResult
import androidx.activity.result.contract.ActivityResultContracts
import androidx.compose.foundation.layout.Column
import androidx.compose.foundation.layout.Row
import androidx.compose.foundation.layout.fillMaxSize
import androidx.compose.foundation.layout.fillMaxWidth
import androidx.compose.foundation.layout.padding
import androidx.compose.foundation.text.KeyboardActions
import androidx.compose.foundation.text.KeyboardOptions
import androidx.compose.material.icons.Icons
import androidx.compose.material.icons.filled.Edit
import androidx.compose.material3.AlertDialog
import androidx.compose.material3.Checkbox
import androidx.compose.material3.ExtendedFloatingActionButton
import androidx.compose.material3.Icon
import androidx.compose.material3.OutlinedTextField
import androidx.compose.material3.Scaffold
import androidx.compose.material3.Text
import androidx.compose.material3.TextButton
import androidx.compose.runtime.Composable
import androidx.compose.runtime.LaunchedEffect
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.platform.LocalContext
import androidx.compose.ui.text.input.ImeAction
import androidx.compose.ui.text.input.KeyboardType
import androidx.compose.ui.text.style.TextAlign
import androidx.compose.ui.unit.dp
import com.edatalia.signplysdkdni.contract.SignplySdkDniResultContract
import kotlinx.coroutines.flow.SharedFlow
import kotlinx.coroutines.flow.collectLatest

@Composable
fun ConfigDniScreen(
    state: ConfigDniState,
    onEvent: (ConfigDniEvent) -> Unit,
    uiEvent: SharedFlow<ConfigDniViewModel.ConfigDniUiEvent>
) {

    val context = LocalContext.current

    val getSignedDocumentWithDni =
        rememberLauncherForActivityResult(contract = SignplySdkDniResultContract()) { result ->
            val fileUri = result.signedUri
            val throwable = result.throwable
            if (fileUri != null) {
                onEvent(ConfigDniEvent.OnSuccesSignedFile(fileUri, context))
            } else if (throwable != null) {
                Toast.makeText(
                    context,
                    throwable.message ?: "Se ha producido un error inesperado",
                    Toast.LENGTH_LONG
                )
                    .show()
            }
        }

    val filePickerLauncher = rememberLauncherForActivityResult(
        contract = ActivityResultContracts.GetContent()
    ) { uri: Uri? ->
        uri?.let {
            onEvent(ConfigDniEvent.OnFilePicked(uri, context))
        }
    }

    LaunchedEffect(key1 = true) {
        uiEvent.collectLatest { event ->
            when (event) {
                is ConfigDniViewModel.ConfigDniUiEvent.LaunchSignplySdkDni -> {
                    getSignedDocumentWithDni.launch(event.params)
                }
            }
        }
    }


    Scaffold(
        floatingActionButton = {
            ExtendedFloatingActionButton(
                icon = {
                    Icon(Icons.Default.Edit, null)
                },
                text = {
                    Text("Firmar con DNI")
                },
                onClick = {
                    if (state.can.length != 6) {
                        Toast.makeText(context, "El CAN debe tener 6 dígitos", Toast.LENGTH_SHORT)
                            .show()
                        return@ExtendedFloatingActionButton
                    }
                    filePickerLauncher.launch("application/pdf")
                }
            )
        }
    ) {

        if (state.showSuccessDialog) {
            AlertDialog(
                title = {
                    Text("Documento firmado correctamente")
                },
                text = {
                    Text("¿Quieres compartirlo?")
                },
                onDismissRequest = {},
                confirmButton = {
                    TextButton(
                        onClick = {
                            onEvent(ConfigDniEvent.OnSuccessDialogShareFileClick(context))
                        }
                    ) {
                        Text("Compartir")
                    }
                },
                dismissButton = {
                    TextButton(
                        onClick = {
                            onEvent(ConfigDniEvent.OnCloseSuccessDialogClick)
                        }
                    ) {
                        Text("Cancelar")
                    }
                }
            )
        }


        Column(
            modifier = Modifier
                .fillMaxSize()
                .padding(16.dp)
                .padding(paddingValues = it)
        ) {
            OutlinedTextField(
                modifier = Modifier.fillMaxWidth(),
                value = state.can,
                onValueChange = { onEvent(ConfigDniEvent.OnCanChanged(it)) },
                label = { Text("Número CAN del DNI") },
                keyboardOptions = KeyboardOptions(
                    keyboardType = KeyboardType.Number,
                    imeAction = ImeAction.Done
                ),
                keyboardActions = KeyboardActions(onDone = {
                    if (state.can.length != 6) {
                        Toast.makeText(context, "El CAN debe tener 6 dígitos", Toast.LENGTH_SHORT)
                            .show()
                        return@KeyboardActions
                    }
                    filePickerLauncher.launch("application/pdf")
                }),
                supportingText = {
                    Text(
                        text = "${state.can.length} / 6",
                        modifier = Modifier.fillMaxWidth(),
                        textAlign = TextAlign.End,
                    )
                }
            )

            Row(modifier = Modifier.fillMaxWidth(), verticalAlignment = Alignment.CenterVertically) {
                Checkbox(checked = state.enableTimestamp, onCheckedChange = { enabled ->
                    onEvent(ConfigDniEvent.OnEnableTimestampChanged(enabled))
                })
                Text("Habilitar sello de tiempo")
            }
        }
    }
}