package com.edatalia.signplysdk.demo

import android.content.Context
import android.content.pm.PackageInfo
import android.content.pm.PackageManager
import androidx.compose.foundation.layout.Column
import androidx.compose.foundation.layout.fillMaxSize
import androidx.compose.foundation.layout.padding
import androidx.compose.material3.ExperimentalMaterial3Api
import androidx.compose.material3.Scaffold
import androidx.compose.material3.Tab
import androidx.compose.material3.TabRow
import androidx.compose.material3.Text
import androidx.compose.material3.TopAppBar
import androidx.compose.runtime.Composable
import androidx.compose.runtime.collectAsState
import androidx.compose.runtime.getValue
import androidx.compose.runtime.mutableIntStateOf
import androidx.compose.runtime.remember
import androidx.compose.runtime.setValue
import androidx.compose.ui.ExperimentalComposeUiApi
import androidx.compose.ui.Modifier
import androidx.compose.ui.platform.LocalContext
import androidx.compose.ui.res.stringResource
import com.edatalia.signplysdk.demo.dni.ConfigDniScreen
import com.edatalia.signplysdk.demo.dni.ConfigDniViewModel
import com.edatalia.signplysdk.demo.legacy.ConfigScreen
import com.edatalia.signplysdk.demo.legacy.ConfigViewModel

@OptIn(ExperimentalComposeUiApi::class, ExperimentalMaterial3Api::class)
@Composable
fun MainScreen(configViewModel: ConfigViewModel, configDniViewModel: ConfigDniViewModel) {

    var selectedTabIndex by remember { mutableIntStateOf(0) }
    val tabs = listOf("Firma Manuscrita", "Firma con DNI")
    val context = LocalContext.current

    Scaffold(
        topBar = {
            TopAppBar(title = { Text(stringResource(R.string.signply_sdk_demo) + " " + getVersionName(context)) })
        }
    ) { paddingValues ->
        Column(modifier = Modifier
            .fillMaxSize()
            .padding(paddingValues)) {
            TabRow(selectedTabIndex = selectedTabIndex) {
                tabs.forEachIndexed { index, title ->
                    Tab(
                        selected = selectedTabIndex == index,
                        onClick = { selectedTabIndex = index },
                        text = { Text(title) }
                    )
                }
            }
            when (selectedTabIndex) {
                0 -> {
                    ConfigScreen(configViewModel)
                }
                1 -> {
                    ConfigDniScreen(
                        configDniViewModel.state.collectAsState().value,
                        onEvent = configDniViewModel::onEvent,
                        uiEvent = configDniViewModel.uiEvent
                    )
                }
            }
        }
    }

}

fun getVersionName(context: Context) : String {
    try {
        val pInfo: PackageInfo =
            context.packageManager.getPackageInfo(context.packageName, 0)
        val version = pInfo.versionName
        return version
    } catch (e: PackageManager.NameNotFoundException) {
        e.printStackTrace()
    }
    return ""
}

