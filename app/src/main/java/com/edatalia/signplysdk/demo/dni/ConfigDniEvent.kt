package com.edatalia.signplysdk.demo.dni

import android.content.Context
import android.net.Uri

sealed class ConfigDniEvent {
    data class OnCanChanged(val tag: String) : ConfigDniEvent()
    data class OnEnableTimestampChanged(val enabled: Boolean) : ConfigDniEvent()
    data class OnFilePicked(val uri: Uri, val context: Context) : ConfigDniEvent()
    data class OnSuccesSignedFile(val uri: Uri, val context: Context) : ConfigDniEvent()
    data object OnCloseSuccessDialogClick : ConfigDniEvent()
    data class OnSuccessDialogShareFileClick(val context: Context) : ConfigDniEvent()
}