package com.edatalia.signplysdk.demo

import android.os.Bundle
import androidx.activity.ComponentActivity
import androidx.activity.compose.setContent
import androidx.activity.viewModels
import androidx.compose.foundation.layout.fillMaxSize
import androidx.compose.material3.MaterialTheme
import androidx.compose.material3.Surface
import androidx.compose.ui.ExperimentalComposeUiApi
import androidx.compose.ui.Modifier
import com.edatalia.signplysdk.demo.dni.ConfigDniViewModel
import com.edatalia.signplysdk.demo.legacy.ConfigViewModel
import com.edatalia.signplysdk.demo.theme.UseCase_JetpackComposeTheme

@ExperimentalComposeUiApi
class MainActivity : ComponentActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        val configViewModel: ConfigViewModel by viewModels()
        val configDniViewModel: ConfigDniViewModel by viewModels()

        setContent {
            UseCase_JetpackComposeTheme {
                Surface(
                    modifier = Modifier.fillMaxSize(),
                    color = MaterialTheme.colorScheme.background
                ) {
                    MainScreen(
                        configViewModel = configViewModel,
                        configDniViewModel = configDniViewModel
                    )
                }
            }
        }
    }
}

