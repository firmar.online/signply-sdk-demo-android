package com.edatalia.signplysdk.demo.legacy

import android.net.Uri
import android.util.Log
import android.widget.Toast
import androidx.activity.compose.rememberLauncherForActivityResult
import androidx.activity.result.contract.ActivityResultContracts
import androidx.compose.foundation.layout.*
import androidx.compose.foundation.rememberScrollState
import androidx.compose.foundation.selection.selectable
import androidx.compose.foundation.text.KeyboardActions
import androidx.compose.foundation.text.KeyboardOptions
import androidx.compose.foundation.verticalScroll
import androidx.compose.material.icons.Icons
import androidx.compose.material.icons.filled.Edit
import androidx.compose.material3.Checkbox
import androidx.compose.material3.ExtendedFloatingActionButton
import androidx.compose.material3.Icon
import androidx.compose.material3.IconButton
import androidx.compose.material3.MaterialTheme
import androidx.compose.material3.OutlinedTextField
import androidx.compose.material3.RadioButton
import androidx.compose.material3.Scaffold
import androidx.compose.material3.Slider
import androidx.compose.material3.Switch
import androidx.compose.material3.Text
import androidx.compose.runtime.Composable
import androidx.compose.runtime.LaunchedEffect
import androidx.compose.ui.Alignment
import androidx.compose.ui.ExperimentalComposeUiApi
import androidx.compose.ui.Modifier
import androidx.compose.ui.platform.LocalContext
import androidx.compose.ui.platform.LocalFocusManager
import androidx.compose.ui.res.painterResource
import androidx.compose.ui.res.stringResource
import androidx.compose.ui.text.input.KeyboardType
import androidx.compose.ui.text.input.PasswordVisualTransformation
import androidx.compose.ui.text.input.VisualTransformation
import androidx.compose.ui.unit.dp
import com.edatalia.signplysdk.contract.SignplySDKResultContract
import com.edatalia.signplysdk.data.SignplySDKPrevDialog
import com.edatalia.signplysdk.data.SignplySDKWidgetType
import com.edatalia.signplysdk.demo.R
import com.edatalia.signplysdkcompose.presentation.PrevDialog
import kotlin.math.roundToInt
import kotlin.math.roundToLong

@ExperimentalComposeUiApi
@Composable
fun ConfigScreen(
    viewModel: ConfigViewModel
) {

    val context = LocalContext.current

    val getSignedDocument =
        rememberLauncherForActivityResult(contract = SignplySDKResultContract()) { result ->
            val fileUri = result.file
            val throwable = result.throwable
            if (fileUri != null) {
                viewModel.launchSIGNplySDK(fileUri)
            } else if (throwable != null) {
                Toast.makeText(context, throwable.toString(), Toast.LENGTH_SHORT)
                    .show()
            }
        }

    fun getSignedDocument(uri: Uri) {
        val signplySDKParams = viewModel.getSignplySDKParams(uri, context)

        try {
            getSignedDocument.launch(signplySDKParams)
        } catch (exception: Exception) {
            Log.e("SIGNplySDKScreen", exception.message ?: "Unexpected error occurred")
        }
    }

    val signLauncher = rememberLauncherForActivityResult(
        contract = ActivityResultContracts.GetContent()
    ) { uri: Uri? ->
        uri?.let {
            viewModel.launchSIGNplySDK(uri)
        }
    }

    LaunchedEffect(key1 = true) {
        viewModel.uiEvent.collect { event ->
            when (event) {
                is ConfigUiEvent.LaunchSignplySdk -> {
                    getSignedDocument(event.uri)
                }
            }
        }
    }

    Scaffold(
        floatingActionButton = {
            ExtendedFloatingActionButton(
                icon = { Icon(Icons.Default.Edit, null) },
                text = { Text("Firmar") },
                onClick = {
                    signLauncher.launch("application/pdf")
                }
            )

        },
        content = { paddingValues ->
            ContentConfigScreen(modifier = Modifier.padding(paddingValues), viewModel = viewModel)
        }
    )
}

@Composable
private fun ContentConfigScreen(modifier: Modifier, viewModel: ConfigViewModel) {

    val focusManager = LocalFocusManager.current
    val scrollState = rememberScrollState()

    Column(
        modifier = modifier
            .fillMaxSize()
            .padding(16.dp)
            .verticalScroll(scrollState),
        verticalArrangement = Arrangement.spacedBy(16.dp)
    ) {

        Row {
            OutlinedTextField(
                value = viewModel.toolbarTitle.value,
                onValueChange = {
                    viewModel.setToolbarTitle(it)
                },
                singleLine = true,
                label = { Text(stringResource(R.string.pdf_title)) },
                keyboardActions = KeyboardActions(
                    onDone = { focusManager.clearFocus() }
                ),
                modifier = Modifier.weight(0.7f)
            )

            Spacer(modifier = Modifier.size(16.dp))

            Column(
                horizontalAlignment = Alignment.CenterHorizontally,
                verticalArrangement = Arrangement.Center,
                modifier = Modifier.weight(0.3f)
            ) {
                Text(text = stringResource(R.string.private_))
                Switch(
                    checked = viewModel.documentPrivate.value,
                    onCheckedChange = { viewModel.setDocumentPrivate(it) },
                )
            }
        }

        Row {
            OutlinedTextField(
                value = viewModel.documentPassword.value,
                onValueChange = {
                    viewModel.setDocumentPassword(it)
                },
                singleLine = true,
                keyboardOptions = KeyboardOptions(keyboardType = KeyboardType.Password),
                visualTransformation = if (viewModel.documentPasswordVisibility.value) {
                    VisualTransformation.None
                } else {
                    PasswordVisualTransformation()
                },
                label = { Text(stringResource(R.string.pdf_password)) },
                keyboardActions = KeyboardActions(
                    onDone = { focusManager.clearFocus() }
                ),
                trailingIcon = {
                    val painterResourceId =
                        if (viewModel.documentPasswordVisibility.value) {
                            R.drawable.ic_baseline_visibility_24
                        } else {
                            R.drawable.ic_baseline_visibility_off_24
                        }
                    IconButton(onClick = {
                        viewModel.toggleDocumentPasswordVisibility()
                    }) {
                        Icon(
                            painterResource(id = painterResourceId),
                            contentDescription = "passwordVisibility"
                        )
                    }
                },
                modifier = Modifier.weight(0.7f)
            )

            Spacer(modifier = Modifier.size(16.dp))

            Column(
                horizontalAlignment = Alignment.CenterHorizontally,
                modifier = Modifier.weight(0.3f)
            ) {
                Text(text = stringResource(R.string.share))
                Switch(
                    checked = viewModel.documentShareable.value,
                    onCheckedChange = { viewModel.setDocumentShareable(it) },
                )
            }
        }

        Row(verticalAlignment = Alignment.CenterVertically) {

            OutlinedTextField(
                value = viewModel.documentSignedName.value,
                onValueChange = {
                    viewModel.setDocumentSignedName(it)
                },
                singleLine = true,
                label = { Text(stringResource(R.string.saved_document_name)) },
                keyboardActions = KeyboardActions(
                    onDone = { focusManager.clearFocus() }
                ),
                modifier = Modifier.weight(0.7f)
            )

            Spacer(modifier = Modifier.size(16.dp))

            OutlinedTextField(
                value = if (viewModel.defaultRenderPage.value != null) {
                    viewModel.defaultRenderPage.value.toString()
                } else {
                    ""
                },
                onValueChange = {
                    viewModel.setDefaultRenderPage(it)
                },
                singleLine = true,
                label = { Text(stringResource(R.string.default_page)) },
                keyboardOptions = KeyboardOptions(keyboardType = KeyboardType.NumberPassword),
                keyboardActions = KeyboardActions(
                    onDone = { focusManager.clearFocus() }
                ),
                modifier = Modifier.weight(0.3f)
            )
        }

        Row(
            horizontalArrangement = Arrangement.spacedBy(16.dp),
            modifier = Modifier.fillMaxWidth()
        ) {
            OutlinedTextField(
                value = viewModel.author.value,
                onValueChange = {
                    viewModel.setAuthor(it)
                },
                singleLine = true,
                label = { Text(stringResource(R.string.author)) },
                keyboardActions = KeyboardActions(
                    onDone = { focusManager.clearFocus() }
                ),
                modifier = Modifier.weight(0.5f)
            )

            OutlinedTextField(
                value = viewModel.reason.value,
                onValueChange = {
                    viewModel.setReason(it)
                },
                singleLine = true,
                label = { Text(stringResource(R.string.reason)) },
                keyboardActions = KeyboardActions(
                    onDone = { focusManager.clearFocus() }
                ),
                modifier = Modifier.weight(0.5f)
            )
        }

        Row(
            horizontalArrangement = Arrangement.spacedBy(16.dp),
            modifier = Modifier.fillMaxWidth()
        ) {
            OutlinedTextField(
                value = viewModel.contact.value,
                onValueChange = {
                    viewModel.setContact(it)
                },
                singleLine = true,
                label = { Text(stringResource(R.string.contact)) },
                keyboardActions = KeyboardActions(
                    onDone = { focusManager.clearFocus() }
                ),
                modifier = Modifier.weight(0.5f)
            )

            OutlinedTextField(
                value = viewModel.location.value,
                onValueChange = {
                    viewModel.setLocation(it)
                },
                singleLine = true,
                label = { Text(stringResource(R.string.location)) },
                keyboardActions = KeyboardActions(
                    onDone = { focusManager.clearFocus() }
                ),
                modifier = Modifier.weight(0.5f)
            )
        }

        Row(
            modifier = Modifier.fillMaxWidth()
        ) {

            Row(
                verticalAlignment = Alignment.CenterVertically,
                modifier = Modifier.weight(1f)
            ) {
                Checkbox(
                    checked = viewModel.requestUserLocation.value,
                    onCheckedChange = { viewModel.setRequestUserLocation(it) },
                )
                Text(
                    text = stringResource(R.string.gps_require),
                )
            }

            Row(
                verticalAlignment = Alignment.CenterVertically,
                modifier = Modifier.weight(1f)
            ) {
                Checkbox(
                    checked = viewModel.topSignatureButtons.value,
                    onCheckedChange = { viewModel.setTopSignatureButtons(it) },
                )
                Text(
                    text = stringResource(R.string.top_buttons),
                )
            }

        }

        OutlinedTextField(
            value = viewModel.captureSignatureTitle.value,
            onValueChange = {
                viewModel.setCaptureSignatureTitle(it)
            },
            singleLine = true,
            label = { Text(stringResource(R.string.signature_capture_title)) },
            keyboardActions = KeyboardActions(
                onDone = { focusManager.clearFocus() }
            ),
            modifier = Modifier.fillMaxWidth()
        )

        WidgetComposable(viewModel = viewModel)

        Spacer(modifier = Modifier.size(8.dp))

        SignatureComposable(viewModel = viewModel)

        WidgetCustomTextComposable(viewModel = viewModel)

        ExtraComposable(viewModel = viewModel)

        if (viewModel.advancedSignature.value) {
            AdvancedSignatureComponent(viewModel = viewModel)
        }

    }
}

@Composable
private fun SignatureComposable(viewModel: ConfigViewModel) {
    Text(stringResource(R.string.signature_color))

    Row(
        horizontalArrangement = Arrangement.spacedBy(4.dp),
        modifier = Modifier.fillMaxWidth(),
        verticalAlignment = Alignment.CenterVertically
    ) {

        viewModel.signatureTextColorValues.forEach { signatureTextColor ->
            Row(
                verticalAlignment = Alignment.CenterVertically,
                modifier = Modifier
                    .selectable(
                        selected = (signatureTextColor == viewModel.signatureTextColor.value),
                        onClick = {
                            viewModel.setSignatureTextColor(signatureTextColor)
                        }
                    )
            ) {
                RadioButton(
                    selected = (signatureTextColor == viewModel.signatureTextColor.value),
                    onClick = { viewModel.setSignatureTextColor(signatureTextColor) }
                )
                Text(
                    text = viewModel.getDisplayNameForSignatureColor(
                        signatureTextColor,
                        LocalContext.current
                    ),
                )
            }
        }
    }


    Column(modifier = Modifier.fillMaxWidth()) {
        Text(text = stringResource(R.string.signature_thickness) + viewModel.signatureThickness.value)
        Slider(
            value = viewModel.signatureThickness.value.toFloat(),
            valueRange = 10f..50f,
            onValueChange = {
                viewModel.setSignatureThickness(it.roundToInt())
            },
            modifier = Modifier.fillMaxWidth()
        )
    }
}

@Composable
private fun WidgetComposable(viewModel: ConfigViewModel) {

    Spacer(modifier = Modifier.size(4.dp))

    Text(
        text = stringResource(R.string.positioning_type)
    )

    Row(
        horizontalArrangement = Arrangement.SpaceBetween,
        modifier = Modifier.fillMaxWidth()
    ) {

        viewModel.widgetTypeValues.forEach { widgetType ->
            Row(
                verticalAlignment = Alignment.CenterVertically,
                modifier = Modifier
                    .selectable(
                        selected = (widgetType == viewModel.widgetType.value),
                        onClick = {
                            viewModel.setWidgetType(widgetType)
                        }
                    )
            ) {
                RadioButton(
                    selected = (widgetType == viewModel.widgetType.value),
                    onClick = { viewModel.setWidgetType(widgetType) }
                )
                Text(
                    text = viewModel.getDisplayNameForWidget(widgetType, LocalContext.current),
                    style = MaterialTheme.typography.labelMedium
                )
            }
        }
    }

    if (viewModel.widgetType.value == SignplySDKWidgetType.Manual) {
        ManualWidgetComponent(viewModel = viewModel)
    }
    if (viewModel.widgetType.value == SignplySDKWidgetType.Fixed) {
        FixedWidgetComponent(viewModel = viewModel)
    }
    if (viewModel.widgetType.value == SignplySDKWidgetType.Field) {
        FieldWidgetComponent(viewModel = viewModel)
    }
    if (viewModel.widgetType.value == SignplySDKWidgetType.Float) {
        FloatWidgetComponent(viewModel = viewModel)
    }

}

@Composable
private fun WidgetCustomTextComposable(viewModel: ConfigViewModel) {

    val focusManager = LocalFocusManager.current

    Column(modifier = Modifier.fillMaxWidth()) {
        Row(verticalAlignment = Alignment.CenterVertically) {
            Checkbox(
                checked = viewModel.requestWidgetCustomText.value,
                onCheckedChange = { viewModel.setRequestWidgetCustomText(it) },
            )
            Text(
                text = stringResource(R.string.require_custom_text),
            )
        }

        Spacer(modifier = Modifier.size(4.dp))

        if (!viewModel.requestWidgetCustomText.value) {
            Row(
                horizontalArrangement = Arrangement.spacedBy(8.dp),
                modifier = Modifier.fillMaxWidth()
            ) {
                OutlinedTextField(
                    value = viewModel.widgetCustomText.value,
                    onValueChange = {
                        viewModel.setWidgetCustomText(it)
                    },
                    singleLine = true,
                    label = { Text(stringResource(R.string.custom_text)) },
                    keyboardActions = KeyboardActions(
                        onDone = { focusManager.clearFocus() }
                    ),
                    modifier = Modifier.weight(0.75f)
                )

                OutlinedTextField(
                    value = if (viewModel.widgetCustomTextFontSize.value != null) {
                        viewModel.widgetCustomTextFontSize.value.toString()
                    } else {
                        ""
                    },
                    onValueChange = {
                        viewModel.setWidgetCustomTextFontSize(it)
                    },
                    singleLine = true,
                    label = { Text(stringResource(R.string.size)) },
                    keyboardOptions = KeyboardOptions(keyboardType = KeyboardType.NumberPassword),
                    keyboardActions = KeyboardActions(
                        onDone = { focusManager.clearFocus() }
                    ),
                    modifier = Modifier.weight(0.25f)
                )
            }
        }
    }
}

@Composable
private fun AdvancedSignatureComponent(viewModel: ConfigViewModel) {

    val focusManager = LocalFocusManager.current

    OutlinedTextField(
        value = viewModel.tspUrl.value,
        onValueChange = {
            viewModel.setTspUrl(it)
        },
        singleLine = true,
        label = { Text(stringResource(R.string.url_tsp)) },
        keyboardActions = KeyboardActions(
            onDone = { focusManager.clearFocus() }
        ),
        modifier = Modifier.fillMaxWidth()
    )

    OutlinedTextField(
        value = viewModel.tspUser.value,
        onValueChange = {
            viewModel.setTspUsername(it)
        },
        singleLine = true,
        label = { Text(stringResource(R.string.user_tsp)) },
        keyboardActions = KeyboardActions(
            onDone = { focusManager.clearFocus() }
        ),
        modifier = Modifier.fillMaxWidth()
    )

    OutlinedTextField(
        value = viewModel.tspPassword.value,
        onValueChange = {
            viewModel.setTspPassword(it)
        },
        singleLine = true,
        keyboardOptions = KeyboardOptions(keyboardType = KeyboardType.Password),
        visualTransformation = if (viewModel.tspPasswordVisibility.value) {
            VisualTransformation.None
        } else {
            PasswordVisualTransformation()
        },
        label = { Text(stringResource(R.string.pass_tsp)) },
        keyboardActions = KeyboardActions(
            onDone = { focusManager.clearFocus() }
        ),
        trailingIcon = {
            val painterResourceId =
                if (viewModel.tspPasswordVisibility.value) {
                    R.drawable.ic_baseline_visibility_24
                } else {
                    R.drawable.ic_baseline_visibility_off_24
                }
            IconButton(onClick = {
                viewModel.toggleTspPasswordVisibility()
            }) {
                Icon(
                    painterResource(id = painterResourceId),
                    contentDescription = "passwordVisibility"
                )
            }
        },
        modifier = Modifier.fillMaxWidth()
    )
}

@Composable
fun ManualWidgetComponent(viewModel: ConfigViewModel) {
    val widgetRatio = (viewModel.widgetManualRatio.value * 10.0f).roundToLong() / 10.0f

    Column(modifier = Modifier.fillMaxWidth()) {
        Text(text = stringResource(R.string.aspect_ratio) + widgetRatio)
        Slider(
            value = viewModel.widgetManualRatio.value,
            valueRange = 1f..4f,
            onValueChange = {
                viewModel.setWidgetManualRatio(it)
            },
            modifier = Modifier.fillMaxWidth()
        )
    }

}

@Composable
private fun FloatWidgetComponent(viewModel: ConfigViewModel) {
    val focusManager = LocalFocusManager.current

    OutlinedTextField(
        value = viewModel.widgetFloatText.value,
        onValueChange = {
            viewModel.setWidgetFloatText(it)
        },
        singleLine = true,
        label = { Text(stringResource(R.string.text_to_find)) },
        keyboardActions = KeyboardActions(
            onDone = { focusManager.clearFocus() }
        ),
        modifier = Modifier.fillMaxWidth()
    )


    Row(
        horizontalArrangement = Arrangement.spacedBy(16.dp),
        modifier = Modifier.fillMaxWidth()
    ) {

        OutlinedTextField(
            modifier = Modifier.weight(0.25f),
            value = if (viewModel.widgetFixedWidth.value != null) {
                viewModel.widgetFixedWidth.value.toString()
            } else {
                ""
            },
            onValueChange = {
                viewModel.setWidgetFixedWidth(it.toIntOrNull())
            },
            singleLine = true,
            label = { Text(stringResource(R.string.width)) },
            keyboardOptions = KeyboardOptions(keyboardType = KeyboardType.NumberPassword),
            keyboardActions = KeyboardActions(
                onDone = { focusManager.clearFocus() }
            )
        )

        OutlinedTextField(
            modifier = Modifier.weight(0.25f),
            value = if (viewModel.widgetFixedHeight.value != null) {
                viewModel.widgetFixedHeight.value.toString()
            } else {
                ""
            },
            onValueChange = {
                viewModel.setWidgetFixedHeight(it.toIntOrNull())
            },
            singleLine = true,
            label = { Text(stringResource(R.string.height)) },
            keyboardOptions = KeyboardOptions(keyboardType = KeyboardType.NumberPassword),
            keyboardActions = KeyboardActions(
                onDone = { focusManager.clearFocus() }
            )
        )

        OutlinedTextField(
            modifier = Modifier.weight(0.25f),
            value = if (viewModel.widgetFieldGapX.value != null) {
                viewModel.widgetFieldGapX.value.toString()
            } else {
                ""
            },
            onValueChange = {
                viewModel.setWidgetFieldGapX(it.toIntOrNull())
            },
            singleLine = true,
            label = { Text(stringResource(R.string.gap_x)) },
            keyboardOptions = KeyboardOptions(keyboardType = KeyboardType.NumberPassword),
            keyboardActions = KeyboardActions(
                onDone = { focusManager.clearFocus() }
            )
        )

        OutlinedTextField(
            modifier = Modifier.weight(0.25f),
            value = if (viewModel.widgetFieldGapY.value != null) {
                viewModel.widgetFieldGapY.value.toString()
            } else {
                ""
            },
            onValueChange = {
                viewModel.setWidgetFieldGapY(it.toIntOrNull())
            },
            singleLine = true,
            label = { Text(stringResource(R.string.gap_y)) },
            keyboardOptions = KeyboardOptions(keyboardType = KeyboardType.NumberPassword),
            keyboardActions = KeyboardActions(
                onDone = { focusManager.clearFocus() }
            )
        )
    }
}


@Composable
private fun FieldWidgetComponent(viewModel: ConfigViewModel) {
    val focusManager = LocalFocusManager.current

    OutlinedTextField(
        value = viewModel.widgetFieldName.value,
        onValueChange = {
            viewModel.setWidgetFieldName(it)
        },
        singleLine = true,
        label = { Text(stringResource(R.string.field_name)) },
        keyboardActions = KeyboardActions(
            onDone = { focusManager.clearFocus() }
        ),
        modifier = Modifier.fillMaxWidth()
    )
}

@Composable
fun FixedWidgetComponent(viewModel: ConfigViewModel) {

    val focusManager = LocalFocusManager.current

    Row(
        horizontalArrangement = Arrangement.spacedBy(4.dp),
        modifier = Modifier.fillMaxWidth(),

        ) {
        OutlinedTextField(
            modifier = Modifier.weight(0.2f),
            value = if (viewModel.widgetFixedPage.value != null) {
                viewModel.widgetFixedPage.value.toString()
            } else {
                ""
            },
            onValueChange = {
                viewModel.setWidgetFixedPage(it.toIntOrNull())
            },
            singleLine = true,
            label = { Text(text = stringResource(R.string.page)) },
            keyboardOptions = KeyboardOptions(keyboardType = KeyboardType.NumberPassword),
            keyboardActions = KeyboardActions(
                onDone = { focusManager.clearFocus() }
            )
        )

        OutlinedTextField(
            modifier = Modifier.weight(0.15f),
            value = if (viewModel.widgetFixedX.value != null) {
                viewModel.widgetFixedX.value.toString()
            } else {
                ""
            },
            onValueChange = {
                viewModel.setWidgetFixedX(it.toIntOrNull())
            },
            singleLine = true,
            label = { Text("X") },
            keyboardOptions = KeyboardOptions(keyboardType = KeyboardType.NumberPassword),
            keyboardActions = KeyboardActions(
                onDone = { focusManager.clearFocus() }
            )
        )

        OutlinedTextField(
            modifier = Modifier.weight(0.15f),
            value = if (viewModel.widgetFixedY.value != null) {
                viewModel.widgetFixedY.value.toString()
            } else {
                ""
            },
            onValueChange = {
                viewModel.setWidgetFixedY(it.toIntOrNull())
            },
            singleLine = true,
            label = { Text("Y") },
            keyboardOptions = KeyboardOptions(keyboardType = KeyboardType.NumberPassword),
            keyboardActions = KeyboardActions(
                onDone = { focusManager.clearFocus() }
            )
        )

        OutlinedTextField(
            modifier = Modifier.weight(0.2f),
            value = if (viewModel.widgetFixedWidth.value != null) {
                viewModel.widgetFixedWidth.value.toString()
            } else {
                ""
            },
            onValueChange = {
                viewModel.setWidgetFixedWidth(it.toIntOrNull())
            },
            singleLine = true,
            label = { Text(stringResource(id = R.string.width)) },
            keyboardOptions = KeyboardOptions(keyboardType = KeyboardType.NumberPassword),
            keyboardActions = KeyboardActions(
                onDone = { focusManager.clearFocus() }
            )
        )

        OutlinedTextField(
            modifier = Modifier.weight(0.2f),
            value = if (viewModel.widgetFixedHeight.value != null) {
                viewModel.widgetFixedHeight.value.toString()
            } else {
                ""
            },
            onValueChange = {
                viewModel.setWidgetFixedHeight(it.toIntOrNull())
            },
            singleLine = true,
            label = { Text(stringResource(id = R.string.height)) },
            keyboardOptions = KeyboardOptions(keyboardType = KeyboardType.NumberPassword),
            keyboardActions = KeyboardActions(
                onDone = { focusManager.clearFocus() }
            )
        )
    }
}

@Composable
private fun ExtraComposable(viewModel: ConfigViewModel) {
    Row(
        modifier = Modifier.fillMaxWidth()
    ) {

        Row(
            verticalAlignment = Alignment.CenterVertically,
            modifier = Modifier.weight(1f)
        ) {
            Checkbox(
                checked = viewModel.autoOpen.value,
                onCheckedChange = { viewModel.setAutoOpen(it) },
            )
            Text(
                text = stringResource(R.string.open_signature_auto),
            )
        }

        Row(
            verticalAlignment = Alignment.CenterVertically,
            modifier = Modifier.weight(1f)
        ) {
            Checkbox(
                checked = viewModel.viewLastPage.value,
                onCheckedChange = { viewModel.setViewLastPage(it) },
            )

            Text(
                text = stringResource(R.string.show_all_pages),
            )
        }
    }

    Row(
        modifier = Modifier.fillMaxWidth()
    ) {

        Row(
            verticalAlignment = Alignment.CenterVertically,
            modifier = Modifier.weight(1f)
        ) {
            Checkbox(
                checked = viewModel.requestUserCertificate.value,
                onCheckedChange = { viewModel.setRequestUserCertificate(it) },
            )
            Text(
                text = stringResource(R.string.user_certificate),
            )
        }

        Row(
            verticalAlignment = Alignment.CenterVertically,
            modifier = Modifier.weight(1f)
        ) {
            Checkbox(
                checked = viewModel.ltv.value,
                onCheckedChange = { viewModel.setLTV(it) },
            )
            Text(
                text = stringResource(R.string.ltv_certificate),
            )
        }
    }

    Row(
        modifier = Modifier.fillMaxWidth()
    ) {

        Row(
            verticalAlignment = Alignment.CenterVertically,
            modifier = Modifier.weight(1f)
        ) {
            Checkbox(
                checked = viewModel.advancedSignature.value,
                onCheckedChange = { viewModel.setAdvancedSignature(it) },
            )
            Text(
                text = stringResource(R.string.advanced_signature),
            )
        }

    }

    Row(
        modifier = Modifier.fillMaxWidth()
    ) {

        Row(
            verticalAlignment = Alignment.CenterVertically,
            modifier = Modifier.weight(1f)
        ) {
            Checkbox(
                checked = viewModel.signatureAllPages.value,
                onCheckedChange = { viewModel.setSignatureOnAllPages(it) },
            )
            Text(
                text = stringResource(R.string.signature_all_pages),
            )
        }
    }

    Row(
        modifier = Modifier.fillMaxWidth()
    ) {

        Row(
            verticalAlignment = Alignment.CenterVertically,
            modifier = Modifier.weight(1f)
        ) {
            Checkbox(
                checked = viewModel.handwrittenSignature.value,
                onCheckedChange = { viewModel.setHandwrittenSignature(it) },
            )
            Text(
                text = stringResource(R.string.handrwritten_signature),
            )
        }
    }

    Row(
        modifier = Modifier.fillMaxWidth()
    ) {
        Row(
            verticalAlignment = Alignment.CenterVertically,
            modifier = Modifier.weight(1f)
        ) {
            Checkbox(
                checked = viewModel.backgroundCanvas.value,
                onCheckedChange = { viewModel.setBackgroundCanvas(it) },
            )
            Text(
                text = stringResource(R.string.background_canvas),
            )
        }
    }
}
