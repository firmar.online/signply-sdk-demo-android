package com.edatalia.signplysdk.demo

import android.content.Context
import java.io.BufferedReader

object Constants {
    const val LICENSE_FILENAME = "license_31_12_2024.lic"
    const val DEFAULT_TSP_URL = "http://tsa.ecosignature.com:8779/"
    const val DEFAULT_APP_BAR_TITLE = "SIGNplySDK "
    const val SIGNATURE_AUTHOR = "Edatalia"
    const val SIGNATURE_REASON = "Demo SIGNplySDK"
    const val SIGNATURE_CONTACT = "www.signply.com"
    const val SIGNATURE_LOCATION = "San Sebastián"

    fun getBase64License(context: Context) : String {
        return context
            .assets
            .open(LICENSE_FILENAME)
            .bufferedReader()
            .use(BufferedReader::readText)
    }
}