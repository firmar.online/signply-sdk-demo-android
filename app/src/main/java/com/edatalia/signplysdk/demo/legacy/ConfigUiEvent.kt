package com.edatalia.signplysdk.demo.legacy

import android.net.Uri

sealed class ConfigUiEvent {
    data class LaunchSignplySdk(val uri: Uri): ConfigUiEvent()
}