package com.edatalia.signplysdk.demo.dni

import android.content.Intent
import android.widget.Toast
import androidx.core.content.FileProvider
import androidx.core.net.toFile
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.edatalia.signplysdk.demo.Constants
import com.edatalia.signplysdkdni.data.SignplySdkDniParams
import kotlinx.coroutines.flow.MutableSharedFlow
import kotlinx.coroutines.flow.MutableStateFlow
import kotlinx.coroutines.flow.asSharedFlow
import kotlinx.coroutines.flow.asStateFlow
import kotlinx.coroutines.launch

class ConfigDniViewModel : ViewModel() {
    private val _state = MutableStateFlow(ConfigDniState())
    val state = _state.asStateFlow()

    private val _uiEvent = MutableSharedFlow<ConfigDniUiEvent>()
    val uiEvent = _uiEvent.asSharedFlow()

    private val canPattern =  Regex("^\\d+\$")

    fun onEvent(event: ConfigDniEvent) {
        when(event) {
            is ConfigDniEvent.OnCanChanged -> {
                if (event.tag.isEmpty() || event.tag.matches(canPattern) ) {
                    if (event.tag.length <= 6) {
                        _state.value = _state.value.copy(
                            can = event.tag
                        )
                    }
                }
            }

            is ConfigDniEvent.OnEnableTimestampChanged -> {
                _state.value = _state.value.copy(
                    enableTimestamp = event.enabled
                )
            }

            is ConfigDniEvent.OnFilePicked -> {
                viewModelScope.launch {
                    val params = SignplySdkDniParams(
                        licenseBase64 = Constants.getBase64License(event.context),
                        uriString = event.uri.toString(),
                        can = state.value.can,
                        enableTimestamp = state.value.enableTimestamp
                    )
                    _uiEvent.emit(ConfigDniUiEvent.LaunchSignplySdkDni(params))
                }
            }

            ConfigDniEvent.OnCloseSuccessDialogClick -> {
                _state.value = _state.value.copy(showSuccessDialog = false)
            }
            is ConfigDniEvent.OnSuccesSignedFile -> {
                val file = event.uri.toFile()
                val uri = FileProvider.getUriForFile(event.context, event.context.packageName + ".fileprovider", file)
                _state.value = _state.value.copy(fileUri = uri, showSuccessDialog = true)
            }
            is ConfigDniEvent.OnSuccessDialogShareFileClick -> {
                try {
                    val shareIntent = Intent()
                    shareIntent.action = Intent.ACTION_SEND
                    shareIntent.putExtra(
                        Intent.EXTRA_STREAM,
                        state.value.fileUri
                    )
                    shareIntent.type = "application/pdf"
                    event.context.startActivity(Intent.createChooser(shareIntent, null))
                } catch (e: Exception) {
                    e.printStackTrace()
                    Toast.makeText(event.context, "Error: No se ha podido compartir el documento", Toast.LENGTH_LONG)
                        .show()
                }

            }


        }
    }

    sealed class ConfigDniUiEvent {
        data class LaunchSignplySdkDni(val params: SignplySdkDniParams): ConfigDniUiEvent()
    }

}

