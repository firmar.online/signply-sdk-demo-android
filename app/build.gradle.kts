plugins {
    alias(libs.plugins.androidApplication)
    alias(libs.plugins.jetbrainsKotlinAndroid)
}

android {
    namespace = "com.edatalia.signplysdk.demo"
    compileSdk = 34

    defaultConfig {
        applicationId = "com.edatalia.signplysdk.demo"
        minSdk = 23
        targetSdk = 34
        versionCode = 17
        versionName = "5.1.2"

        testInstrumentationRunner = "androidx.test.runner.AndroidJUnitRunner"
        vectorDrawables {
            useSupportLibrary = true
        }
    }

    buildTypes {
        release {
            isMinifyEnabled = false
            proguardFiles(
                getDefaultProguardFile("proguard-android-optimize.txt"),
                "proguard-rules.pro"
            )
        }
    }
    compileOptions {
        sourceCompatibility = JavaVersion.VERSION_1_8
        targetCompatibility = JavaVersion.VERSION_1_8
    }


    kotlinOptions {
        jvmTarget = "1.8"
    }
    buildFeatures {
        compose = true
        viewBinding = true
    }
    composeOptions {
        kotlinCompilerExtensionVersion = "1.5.4"
    }
    packaging {
        resources {
            excludes += "/META-INF/{AL2.0,LGPL2.1}"
        }
    }
}

dependencies {


    // THIS USE libs.versions.toml located in gradle folder.
    /* NEEDED FOR SIGNply SDK */
    implementation(libs.signply.sdk)
    implementation(libs.androidx.appcompat)
    implementation(libs.androidx.constraintlayout)
    /* END SIGNply SDK */

    /* NEEDED FOR DNIe OMIT IF NOT USE*/
    implementation("com.airbnb.android:lottie-compose:6.1.0")
    implementation(files("libs/dniedroid-release.aar"))
    implementation("org.bouncycastle:bcprov-jdk15on:1.67")
    implementation("org.bouncycastle:bcpkix-jdk15on:1.67")
    /* END DNIe */

    implementation(libs.androidx.material3)
    implementation(libs.androidx.core.ktx)
    implementation(libs.androidx.lifecycle.runtime.ktx)
    implementation(libs.androidx.activity.compose)
    implementation(platform(libs.androidx.compose.bom))
    implementation(libs.androidx.ui)
    implementation(libs.androidx.ui.graphics)
    implementation(libs.androidx.ui.tooling.preview)

    testImplementation(libs.junit)
    androidTestImplementation(libs.androidx.junit)
    androidTestImplementation(libs.androidx.espresso.core)
    androidTestImplementation(platform(libs.androidx.compose.bom))
    androidTestImplementation(libs.androidx.ui.test.junit4)
    debugImplementation(libs.androidx.ui.tooling)
    debugImplementation(libs.androidx.ui.test.manifest)
}