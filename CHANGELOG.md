# Changelog from v3 Android

## [3.0.1]
- 3.0.1 Adobe 8 compatibility

## [3.1.0]
- Updates signing default cert due to its near expiration date

## [3.1.1]
- Fixes behavior widget with fixed page (out of bounds ex: 99)

## [3.1.2]
- Better strategy on LTV + custom signing certs

## [3.2.0]
- Minor fixes author and reason (avoid nulls)

## [3.2.2]
- Fixes widgetPage 0 and signAllPages true

## [3.2.3]
- Fixes SignatureName + field positioning (was breaking old signatures)

## [3.4.0]
- Removes kotlinx-serialization mandatory (uses Java serialization defaults)
- Removes material design mandatory

## [3.4.1]
- Quality signature improvements with white pixels

## [3.4.2]
- Minor restyling signing dialog, minor fixes cursor

## [4.0.0-beta]
- DNIe support

## [4.3.0]
- Adds optional handwritten signature 
- Adds optional dialog prev signature
- Adds optional background image signature in fixed and field positioning

## [5.1.1]
- Adds PdfViewerScreen (Jetpack Compose) with a partial compatibility with signply sdk params
- Allows to position the signature when handwritten signature is false

## [5.1.2]
- Adds optional capture signature subtitle text and height


# Migration Guide

## Update from 4.x to 5.x

The repository is now updated from  
~~`com.github.MovilidadEdatalia:signply-sdk-release-android`~~  
to  
**`com.github.MovilidadEdatalia:signply-sdk-releases-android`**. (note the added **s**).


## Update from 3.1.0 to 3.2.3

Uses library contained in jitpack instead local aab

In app/build.gradle replace

```
implementation files('libs/*aar_name*')
```
by
```
implementation "com.github.MovilidadEdatalia:signply-sdk-release-android:3.2.3"
```
It's also necessary to add jitpack as repositories in settings.gradle

```
...
google()
mavenCentral()
maven { url "https://jitpack.io" }
...
```
Sync gradle files and you are ok.

## Update from 3.2.3 to 3.4.2

### Theming
Now the app uses by default Theme.AppCompat.DayNight.NoActionBar and not MaterialComponents.
This theme uses this two colors. The library works with night colors also. For example in src/main/res/values/colors.xml

```
<?xml version="1.0" encoding="utf-8"?>
<resources>
    <color name="signply_sdk_primary">#0057A0</color>
    <color name="signply_sdk_on_primary">#FFFFFF</color>
</resources>
```

The same operation could be done for the folder dark mode (values-night) by choosing other colors.

*Before v3.4.0*
The SDK uses a custom Theme that inherits from Theme.MaterialComponents.DayNight.DarkActionBar.
These can be overwritten from the main application.
https://developer.android.com/guide/topics/ui/look-and-feel/themes
https://material.io/develop/android/docs/getting-started

If you want to override the theme, you can always apply a style for an activity as follows:

In the AndroidManifest.xml:
```xml
<activity          
android:name="com.edatalia.signplysdk.presentation.SignplySDKLauncher"
android:theme="@style/Theme.CustomTheme" />
```

In themes.xml inside values:
```xml
<style name="Theme.CustomTheme" parent="Theme.MaterialComponents.DayNight.DarkActionBar">
        <!-- Primary brand color. -->
        <item name="colorPrimary">#0057A0</item>
        <item name="colorPrimaryVariant">#002F71</item>
        <item name="colorOnPrimary">@android:color/white</item>
        <!-- Secondary brand color. -->
        <item name="colorSecondary">#0057A0</item>
        <item name="colorSecondaryVariant">#002F71</item>
        <item name="colorOnSecondary">@android:color/white</item>
        <!-- Status bar color. -->
</style>
```

The same operation could be done for the folder dark mode (values-night) by choosing other colors.


### Dependencies

*Starting with v3.4.0* you only need this dependencies. Library is contained in jitpack instead local aab.

```groovy
implementation 'androidx.appcompat:appcompat:1.6.1'
implementation 'androidx.constraintlayout:constraintlayout:2.1.4'
implementation 'com.github.MovilidadEdatalia:signply-sdk-release-android:3.4.2'
```

*In v3.2.3 you needed this dependencies in app/build.gradle*

```groovy
implementation 'androidx.appcompat:appcompat:1.6.1' 
implementation 'com.google.android.material:material:1.4.0'
implementation 'org.jetbrains.kotlinx:kotlinx-serialization-json:1.3.2'
implementation 'org.jetbrains.kotlinx:kotlinx-coroutines-android:1.7.2'
implementation 'androidx.constraintlayout:constraintlayout:2.1.4'
implementation 'com.github.MovilidadEdatalia:signply-sdk-release-android:3.2.3'
```











